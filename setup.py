
from setuptools import setup, find_packages

kwargs = {'author': '',
 'author_email': '',
 'description': '',
 'download_url': '',
 'include_package_data': True,
 'install_requires': ['numpy', 'scipy'],
 'keywords': [],
 'license': '',
 'maintainer': '',
 'maintainer_email': '',
 'name': 'hypgrid2dwrapper',
 'packages': ['hypgrid2dwrapper'],
 'version': '0.1',
 'zip_safe': False}


setup(**kwargs)

