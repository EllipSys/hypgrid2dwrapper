# HypGrid2DWrapper

`HypGrid2DWrapper` is a Python wrapper for the 2D hyperbolic mesh generator HypGrid2D,
written by Niels N. Sørensen.<sup>[1](#Sorensen1998b)</sup>

## License

HypGrid2D is a licensed software, and not included in this repository, 
and therefore needs to be installed separately.
To gain access to this code contact the main developer Niels N. Sørensen (nsqr@dtu.dk).

## Installation

To install the wrapper simply execute the command:

    $ python setup.py develop
    
To test that this module and the HypGrid2D code has been installed correctly
run the tests:

    $ cd hypgrid2dwrapper/test
    $ python test_hypgrid2dwrapper.py
    
or if you have `nose` installed simply:

    $ nosetests .
    
from the repository root directory.

## References
<a name="Sorensen1995">1</a>:
Niels N. Sørensen,
*HypGrid2D - a 2-D Mesh Generator*,
Risø-R-1035-EN, Risø National Laboratory,
Roskilde, Denmark, 1995