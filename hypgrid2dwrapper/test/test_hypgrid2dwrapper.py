
import unittest
import shutil
import os
import pkg_resources
import numpy as np
from hypgrid2dwrapper.hypgrid2dwrapper import HypGrid2DWrapper

PATH = pkg_resources.resource_filename('hypgrid2dwrapper', 'test')

x65 = np.array([ -1.3159134862241663e-05,  -2.5612608188565971e-05,
        -4.0801872242092143e-05,  -5.9275203659174077e-05,
        -8.1743008793271470e-05,  -1.0906948007014608e-04,
        -1.4230590189217242e-04,  -1.8273111813440715e-04,
        -2.3190076874351717e-04,  -2.9170672938913853e-04,
        -3.6444974691487791e-04,  -4.5292574247025641e-04,
        -5.6053044406340264e-04,  -6.9138244701595045e-04,
        -8.5046916161608170e-04,  -1.0438128487084239e-03,
        -1.2786596486562035e-03,  -1.5636855803429614e-03,
        -1.9092183943666312e-03,  -2.3274656516136085e-03,
        -2.8323475546945787e-03,  -3.4402176582931016e-03,
        -4.1699876125296373e-03,  -5.0435909661206849e-03,
        -6.0866383877346318e-03,  -7.3291721502181132e-03,
        -8.8064598719209634e-03,  -1.0559622010867334e-02,
        -1.2635990413325904e-02,  -1.5089193901058453e-02,
        -1.7978794073967491e-02,  -2.1369611909107519e-02,
        -2.5330476145531473e-02,  -2.9932259325322820e-02,
        -3.5245094434245582e-02,  -4.1334346635196750e-02,
        -4.8255303475536637e-02,  -5.6046857424093675e-02,
        -6.4724211962663411e-02,  -7.4271455983087112e-02,
        -8.4635355043577387e-02,  -9.5720985498018635e-02,
        -1.0739142653863203e-01,  -1.1947169288838277e-01,
        -1.3175752156248385e-01,  -1.4658003247367088e-01,
        -1.6748321131937749e-01,  -1.9633158838707232e-01,
        -2.3608866291726538e-01,  -2.9084279948273045e-01,
        -3.6624518261637368e-01,  -4.7010318403522094e-01,
        -6.1317241404524681e-01,  -8.1017517171817766e-01,
        -1.0810198252115273e+00,  -1.4520780427244220e+00,
        -1.9572320237743110e+00,  -2.6381720355959812e+00,
        -3.5430719619139710e+00,  -4.7223393364993829e+00,
        -6.2201003841734952e+00,  -8.0609878080832900e+00,
        -1.0234366698302251e+01,  -1.2681836321440258e+01,
        -1.5296026194193525e+01])


gcf = [
 '#---------  meshtype to generate\n',
 'meshtype  omesh-bcdyn\n',
 'block-size  32 \n',
 '#---------  number of mesh points\n',
 'meshpoints  128 64 \n',
 'normal-coarsening  1\n',
 '#--------- domain quantities \n',
 'domain-height  20.d0  1.d-5\n',
 'surface-distribution 3 0.0 0.00125d0 0.0d0 0.5 0.0015 0.5d0 1.0 0.00125d0 1.0d0\n',
 'normal-distribution 3 0.0 1d-5 0.0 0.007 -1 0.7 1.0d0 -1.0d0 1.d0\n',
 '#--------- boundary layer mesh\n',
 'stretching-function  tanh\n',
 '# stretching-function sinh\n',
 '#--------- surface representation  \n',
 '# surface-representation linear\n',
 '#-------- volume blend factor\n',
 'volume-blend  0.002d0\n',
 '# volume-blend 0.5d0 \n',
 '#-------- dissipation factor \n',
 'dissipation-factor  1.0d0  3.d0\n',
 '#-------- explicit smoothing\n',
 '# explicit-smoothing 12 0.03d0\n',
 '#-------- wake geometry \n',
 '# wake-geometry 64 15.d0\n',
 '#-------- flow angle\n',
 '# flow-angle 20.d0\n',
 '# flow-angle 10.d0\n',
 '#-------- wake angle\n',
 '# wake-angle 0.d0\n',
 '#-------- omesh outelet portion\n',
 '# omesh-outlet  64  64\n',
 '#-------- wake contractionfor omesh\n',
 'wake-contraction  0.175d0\n',
 '#-------- projection towards outer boundary\n',
 '# projection 20 35 50 0.d0 15.d0 \n',
 '#-------- adjust point distribution adaptivly on outer bc\n',
 '# adjust-outer false\n',
 '# move-omesh-cut  NNSCUT\n']

def test_with_gcf():

    fid = open('gcf.dat', 'w')
    shutil.copy(os.path.join(PATH, 'prof_inp.dat'), 'prof.dat')
#   shutil.copy('prof_inp.dat', 'sf.dat')
    for line in gcf:
        fid.write(line)

    fid.close()
    h = HypGrid2DWrapper()
    h.from_gcf = True
    h.execute()
    return h

def test_without_gcf():

    prof = np.loadtxt(os.path.join(PATH, 'prof_inp.dat'))
    nprof = prof.shape[0]

    h = HypGrid2DWrapper()
    h.from_gcf = False


    h.control.bdim = 32
    h.control.ni = 129
    h.control.nj = 65
    h.control.meshtype = 20
    h.control.bctype = 22

    h.control.wake_contract = 0.175e0
    h.control.diss_fac = 1.
    h.control.diss_fac2 = 3.
    h.control.stretching_function = 1
    h.control.NrNormalDist = 3
    h.control.NrSurfaceDist = 3
    h.control.SurfaceDistInp = np.array([[0.0, 0.00125e0, 0.0e0],
                                [0.5, 0.0015, 0.5e0],
                                [1.0, 0.00125e0, 1.0e0]])
    h.control.NormalDistInp = np.array([[0.0, 1e-5, 0.0],
                               [0.007, -1., 0.7],
                               [1.0e0, -1.0e0, 1.e0]])

    h.surface.nprof = nprof
    h.surface.xprof = prof[:,0]
    h.surface.yprof = prof[:,1]
#   h.surface.lsurf = True
#   h.surface.xsurf = prof[:,0]
#   h.surface.ysurf = prof[:,1]


    h.geometry.vol_blend = 0.002
    h.geometry.domain_height = 20.
    h.geometry.cell_height = 1.e-5
    h.execute()

    return h

class HypGrid2DWrapperTestCase(unittest.TestCase):

    def setUp(self):

        try:
            os.mkdir('test_dir')
        except:
            pass
        os.chdir('test_dir')

    def test_with_gcf(self):

        h = test_with_gcf()
        for i in range(3):
            h.execute()
            self.assertEqual(np.testing.assert_array_almost_equal(h.vertices.x[65, :], x65, decimal=5), None)

    def test_without_gcf(self):

        h = test_without_gcf()
        for i in range(3):
            h.execute()
            self.assertEqual(np.testing.assert_array_almost_equal(h.vertices.x[65, :], x65, decimal=6), None)

    def tearDown(self):

        os.chdir('..')
        shutil.rmtree('test_dir')

if __name__ == "__main__":
    unittest.main()
    # h2 = test_without_gcf()
    # h2 = test_with_gcf()

    # from PGL.main.domain import Domain, Block
    # d = Domain()
    # d.add_blocks(Block(h2.vertices.x, h2.vertices.y, np.zeros(h2.vertices.y.shape)))
