__all__ = ['HypGrid2DWrapper']

import imp
import os
import numpy as np


class Geometry(object):

    def __init__(self):

        # height of first cell
        self.cell_height = 1.e-5
        # approximate height of domain
        self.domain_height = 10.e0
        # approxmiate wake lenght
        self.wake_lenght = 1.e0
        # volume blending factor
        self.vol_blend = 0.e0
        # number of points in wake, and outlet part of wake
        self.iwake = False
        self.ioutu = False
        self.ioutl = False
        # move cut of ogrig
        self.movecut = False
        # normal coarsing
        self.inormalcoarsing = True

class Control(object):

    def __init__(self):


        # number of cells in ksi,and eta
        self.ni = 256
        self.nj = 128
        self.bdim = 32
        # mesh type and distribution function
        self.meshtype = 1
        self.stretching_function = 1
        # stretching functions
        self.NrSurfaceDist = 0
        self.NrNormalDist = 0
        self.NrHeight = 0
        self.SurfaceDistInp = []
        self.NormalDistInp = []
        self.HeightInp = []
        # number smooth sweeps after solution
        self.nsmooth = 0
        # smooth factor
        self.alpha0 = 0.e0
        # numerical dissipation factor
        self.diss_fac = 1.e0
        self.diss_fac2 = 0.e0
        # profile representation (spline or linear)
        self.lspline = 1
        # profile rotation
        self.flow_angle = 0.e0
        self.wake_angle = 0.e0
        # wake contraction
        self.wake_contract = 0.e0


class Surface(object):

    def __init__(self):

        # surface description
        self.nprof = 0
        self.xprof = np.array([])
        self.yprof = np.array([])
        # distribution function
        self.ndist = 0
        self.xdist = np.array([])
        self.ydist = np.array([])
        # wake description
        self.lwake = False
        self.nwake = 0
        self.xwake = np.array([])
        self.ywake = np.array([])
        # surface grid
        self.lsurf = False
        self.lsurfout = False
        self.ladjustout = False
        self.nsurf = 0
        self.xsurf = np.array([])
        self.ysurf = np.array([])
        self.xsurfo = np.array([])
        self.ysurfo = np.array([])

class Projectioncontrol(object):

    def __init__(self):

        self.GPminIDX = 0
        self.GPmaxIDX = 0
        self.GPintIDX = 0
        self.GPminAngle = 0.e0
        self.GPmaxAngle = 0.e0

class Vertices(object):

    def __init__(self):

        self.x = np.array([])
        self.y = np.array([])
        self.v = np.array([])
        self.h = np.array([])
        self.attr = np.array([], dtype=int)
        self.xb = np.array([])
        self.yb = np.array([])
        self.attrb = np.array([], dtype=int)

class HypGrid2DWrapper(object):
    """
    Python wrapper for the 2D hyperbolic mesh generation code HypGrid2D
    developed by Niels N. Sorensen, DTU Wind Energy.
    """

    def __init__(self, prefix=''):
        """
        """

        try:
            if prefix == 'c_':
                from HypGrid2D import c_HypGrid2D as HypGrid2D
            else:
                from HypGrid2D import HypGrid2D
            self.HypGrid2D = HypGrid2D
            lHypGrid2D = True
        except:
            raise Warning('HypGrid2D could not be imported, check that you have installed it correctly')
            lHypGrid2D = False

        self.from_gcf = True
        self._init_called = False

        self.geometry = Geometry()
        self.control = Control()
        self.surface = Surface()
        self.projectioncontrol = Projectioncontrol()
        self.vertices = Vertices()

    def update_from_hypgrid(self):

        # retrieve vertices from HypGrid2D
        for name in self.vertices.__dict__.keys():
            if hasattr(self.HypGrid2D.h2d_vertices, name):
                val = getattr(self.HypGrid2D.h2d_vertices, name)
                setattr(self.vertices, name, val)

#       for name in self.geometry.__dict__.keys():
#           val = getattr(self.HypGrid2D.geometry, name)
#           setattr(self.geometry, name, val)

#       for name in self.control.__dict__.keys():
#           val = getattr(self.HypGrid2D.control, name)
#           setattr(self.control, name, val)

#       for name in self.surface.__dict__.keys():
#           val = getattr(self.HypGrid2D.surface, name)
#           setattr(self.surface, name, val)

#       for name in self.projectioncontrol.__dict__.keys():
#           val = getattr(self.HypGrid2D.projectioncontrol, name)
#           setattr(self.projectioncontrol, name, val)

    def update_hypgrid(self):

        for name, val in self.geometry.__dict__.iteritems():
            if isinstance(val, (list, np.ndarray)):
                if len(val) > 0:
                    setattr(self.HypGrid2D.h2d_geometry, name, val)
            else:
                setattr(self.HypGrid2D.h2d_geometry, name, val)

        for name, val in self.control.__dict__.iteritems():
            if isinstance(val, (list, np.ndarray)):
                if len(val) > 0:
                    setattr(self.HypGrid2D.h2d_control, name, val)
            else:
                setattr(self.HypGrid2D.h2d_control, name, val)

        for name, val in self.surface.__dict__.iteritems():
            if isinstance(val, (list, np.ndarray)):
                if len(val) > 0:
                    setattr(self.HypGrid2D.h2d_surface, name, val)
            else:
                setattr(self.HypGrid2D.h2d_surface, name, val)

        for name, val in self.projectioncontrol.__dict__.iteritems():
            if isinstance(val, (list, np.ndarray)):
                if len(val) > 0:
                    setattr(self.HypGrid2D.h2d_projectioncontrol, name, val)
            else:
                setattr(self.HypGrid2D.h2d_projectioncontrol, name, val)

    def execute(self):
        """ do your calculations here """

        self.HypGrid2D.h2d_deallocate_all()

        if self.from_gcf:
            # read gcf.dat
            self.HypGrid2D.h2d_read_inputs()
        else:
            # set inputs from Python
            self.update_hypgrid()
            if not self._init_called: self.HypGrid2D.h2d_initialize()

        # run HypGrid2D
        self.HypGrid2D.h2d_run()

        # retrieve outputs
        self.update_from_hypgrid()
